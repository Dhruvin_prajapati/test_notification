import 'dart:convert';

import 'package:http/http.dart' as http;

class LoginClass{
  final String _url = "http://firstoffer.co.in/api/";
  _setHeader() =>{
    'Content-type' : 'application/json',
        'Accept' : 'application/json',
  };

  getLogin(data,apiurl) async{
    var fullUrl = _url + apiurl;
    return await http.post(
      fullUrl,body: jsonEncode(data),
      headers: _setHeader()
    );
  }
  
}