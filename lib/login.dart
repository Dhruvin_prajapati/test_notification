import 'dart:convert';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firstoffer/Home.dart';
import 'package:firstoffer/model/getLogin.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:shared_preferences/shared_preferences.dart';
final FirebaseAuth _auth = FirebaseAuth.instance;
Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
   print('hell $message');
   return Future.value(message);

   // Or do other work.
 }

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  String mobileNo = '';
  int EnrollmentNo = null;
  String verification = 'Hello';
  String verificationId;
  String smsCode;
  TextEditingController _numberControllor = TextEditingController();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final FirebaseMessaging _fcm = FirebaseMessaging();
  var platformChannelSpecifics;
  
  @override
  void initState(){
    super.initState();
    
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    var android = new AndroidInitializationSettings('@mipmap/ic_launcher');
    var iOS = new IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    var initSetttings = new InitializationSettings(android, iOS);
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'Bingeworthy-Dev',
      'Bingeworthy-Dev',
      'Bingeworthy-Dev',
      importance: Importance.Max,
      priority: Priority.High,
    );
    var iOSPlatformChannelSpecifics = IOSNotificationDetails(
      presentAlert: true,
      presentBadge: true,
      presentSound: true,
    );
    platformChannelSpecifics = NotificationDetails(
      androidPlatformChannelSpecifics,
      iOSPlatformChannelSpecifics,
    );
    flutterLocalNotificationsPlugin.initialize(initSetttings,
        onSelectNotification: onSelectNotification);

    flutterLocalNotificationsPlugin.initialize(initSetttings,
        onSelectNotification: onSelectNotification);
    _fcm.getToken().then((token) {
      update(token);
    });
    _fcm.configure(
      onLaunch: (Map<String, dynamic> msg) async {
        print(" onLaunch called ${(msg)}");
      },
      onResume: (Map<String, dynamic> msg) async {
        print(" onResume called ${(msg)}");
      },
      onBackgroundMessage: Platform.isIOS ? null : myBackgroundMessageHandler,
      onMessage: (Map<String, dynamic> msg) async {
        print(" onMessage called ${(msg['notification'])}");
        showNotification(msg['notification']['title'],msg['notification']['body']);
      },
    );
    _fcm.onIosSettingsRegistered
        .listen((IosNotificationSettings setting) {
      print('IOS Setting Registed');
    });

  }
  
  update(String token) {
    print("tokem start hear"+token);
    setState(() {});
  }

  showNotification(String title, String message) async {
    print(title+message);
    var android = new AndroidNotificationDetails(
        'channel id', 'channel NAME', 'CHANNEL DESCRIPTION',
        priority: Priority.High,importance: Importance.Max
    );
    var iOS = new IOSNotificationDetails();
    var platform = new NotificationDetails(android, iOS);
    await flutterLocalNotificationsPlugin.show(
        0, '$title', '$message', platform,
        payload: 'nooooooo');
  }
  Future onSelectNotification(String payload) {
    debugPrint("payload : $payload");
    showDialog(
      context: context,
      builder: (_) => new AlertDialog(
        title: new Text('Notification'),
        content: new Text('$payload'),
      ),
    );
  }
  _showSnackBar(String str) {
    final snackBar = new SnackBar(
        content: new Text(str),
        duration: new Duration(seconds: 5),
        backgroundColor: Colors.orange,
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    await flutterLocalNotificationsPlugin
        .show(0, title, body, platformChannelSpecifics,
        payload: 'Default_Sound')
        .catchError((error) {
      print(error);
    });
  }

  @override
  void dispose() {
    _numberControllor.dispose();
    super.dispose();
  }

  authanticate() {
    final PhoneCodeAutoRetrievalTimeout autoRetrival = (String verId) {
      this.verificationId = verId;
    };

    final PhoneCodeSent smsCodeSent = (String verId, [int forceCodeResend]) {
      print('Verification Code Sent');
      this.verificationId = verId;
      smsCodeDialog(context).then((value) {});
    };

    final PhoneVerificationCompleted verificarionCompelete = (user) {
      print('User verified ');
    };

    final PhoneVerificationFailed verificationFail = (exception) {
      print(exception.message);
    };
    FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: "+91"+this.mobileNo,
        codeAutoRetrievalTimeout: autoRetrival,
        codeSent: smsCodeSent,
        timeout: Duration(seconds: 30),
        verificationCompleted: verificarionCompelete,
        verificationFailed: verificationFail);
  }

  Future<bool> smsCodeDialog(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            title: Text('Enter sms Code'),
            content: TextField(
              onChanged: (value) {
                this.smsCode = value;
              },
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              new FlatButton(
                child: Text('Done'),
                onPressed: () {
                  FirebaseAuth.instance.currentUser().then((user) async {
                    if (user != null) {
                      saveNamePrefrence(this.mobileNo,this.EnrollmentNo);
                    } else {
                      Navigator.of(context).pop();
                      signIn();
                    }
                  });
                },
              )
            ],
          );
        });
  }

  signIn() async {
    final AuthCredential credential = PhoneAuthProvider.getCredential(
      verificationId: verificationId,
      smsCode: smsCode,
    );
    final AuthResult authResult = await _auth.signInWithCredential(credential);
    final FirebaseUser user = authResult.user;
    print('User Id : ' + user.uid);
  }

  Future<bool> saveNamePrefrence(String mobileNo,int enrollmentNo) async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString('mobileNo', mobileNo);
    pref.setInt('EnrollmentNo', enrollmentNo);
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
        HomePage()), (Route<dynamic> route) => false);
  }
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextField(
                controller: _numberControllor,
                keyboardType: TextInputType.phone,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Number',
                    hintText: 'Number'),
              ),
              MaterialButton(
                onPressed: () async {
                  mobileNo = _numberControllor.text;
                  var data = {
                      'number':mobileNo,
                    };
                  var res = await LoginClass().getLogin(data,'number_check');
                  var body = json.decode(res.body);

                   if(body.length == 1){
                     if(body[0]['IsVerified'] == '0' && body[0]['IsActive'] == '1'){
                       String str = "you are not Verified";
                       _showSnackBar(str);
                     }
                     else{
                       this.EnrollmentNo = body[0]['EnrollmentNo'];
                       authanticate();
                     }
                   }
                   else{
                     String str = "Recored Not Found";
                     _showSnackBar(str);
                   }
                },
                child: Text('Verify'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
