import 'package:firstoffer/login.dart';
import 'package:firstoffer/Home.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';




Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  int EnrollmentNo = prefs.getInt('EnrollmentNo');
  print(EnrollmentNo);
  runApp(MaterialApp(home: EnrollmentNo == null ? Login() : HomePage()));
}

